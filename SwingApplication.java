import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class SwingApplication extends Frame implements ActionListener{
    int numClicks = 0;
    Label label;
    JButton button1;

    public SwingApplication(){
        this.button1 = new JButton("I'm a swing button");
        this.button1.addActionListener(this);
        this.label = new Label("Label Prefix = "+numClicks);
        this.add(label, BorderLayout.CENTER);
        this.add(button1, BorderLayout.NORTH);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        numClicks++;
        label.setText("Label Prefix = "+numClicks);
    }
 
    public static void main(String [] args){
        SwingApplication myGUI = new SwingApplication();
        myGUI.setSize(200, 200);
        myGUI.setVisible(true);
    }
}